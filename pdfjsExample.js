
const { degrees, PDFDocument, rgb, StandardFonts } = PDFLib
var pdfBytes = null;
var existingPdfBytes = null;


async function downloadPdf() {
    download(pdfBytes, "pdf-lib_modification_example.pdf", "application/pdf");
}

async function getPdf() {

    if (existingPdfBytes === null){
        const url = 'https://pdf-lib.js.org/assets/with_update_sections.pdf'

        // const url = 'https://unec.edu.az/application/uploads/2014/12/pdf-sample.pdf'
        existingPdfBytes = await fetch(url).then(res => res.arrayBuffer())
    }
    return existingPdfBytes;
}

async function modifyPdf(existingPdfBytes) {
    // Fetch an existing PDF document
    const b = await getPdf();

    // Load a PDFDocument from the existing PDF bytes
    const pdfDoc = await PDFDocument.load(b)

    // Embed the Helvetica font
    const helveticaFont = await pdfDoc.embedFont(StandardFonts.Helvetica)

    // Get the first page of the document
    const pages = pdfDoc.getPages()
    const firstPage = pages[0]

    // Get the width and height of the first page
    const { width, height } = firstPage.getSize()

    const color = rgb(
        document.getElementById('myRed').value/100,
        document.getElementById('myGreen').value/100,
        document.getElementById('myBlue').value/100,
    )
    // Draw a string of text diagonally across the first page
    firstPage.drawText(document.getElementById('text').value, {
        x: width * parseInt(document.getElementById('myX').value)/100,
        y: height - (height * parseInt(document.getElementById('myY').value) / 100),
        size: 50,
        font: helveticaFont,
        color: color,
        rotate: degrees( parseInt(document.getElementById('myRotation').value) ),
    })

    // Serialize the PDFDocument to bytes (a Uint8Array)
    const pdfB = await pdfDoc.save()

    pdfBytes = structuredClone(pdfB);
    renderPdf(pdfB);
}

async function renderPdf(pdfB){
    var loadingTask = pdfjsLib.getDocument({ data: pdfB });
    loadingTask.promise.then(function (pdf) {
        console.log('PDF loaded');

        // Fetch the first page
        var pageNumber = 1;
        pdf.getPage(pageNumber).then(function (page) {
            console.log('Page loaded');

            var scale = document.getElementById("myScale").value;
            var viewport = page.getViewport({ scale: scale });

            // Prepare canvas using PDF page dimensions
            var canvas = document.getElementById('the-canvas');
            var context = canvas.getContext('2d');
            canvas.height = viewport.height;
            canvas.width = viewport.width;

            // Render PDF page into canvas context
            var renderContext = {
                canvasContext: context,
                viewport: viewport
            };
            var renderTask = page.render(renderContext);
            renderTask.promise.then(function () {
                console.log('Page rendered');
                modifyFinished = null;
            });
        });
    }, function (reason) {
        // PDF loading error
        console.error(reason);
        modifyFinished = null;
    });
}

var modifyFinished = null;
function reModify(date){
    console.log("Modify:", modifyFinished);
    if (modifyFinished == null) {
        modifyFinished = new Date();
        modifyPdf();
        return;
    } else if (date instanceof Date && modifyFinished > date){
        return;
    }
    window.setTimeout(reModify, 100, modifyFinished);
}

[
    'text',
    'myRed',
    'myGreen',
    'myBlue',
    'myScale',
    'myRotation',
    'myX',
    'myY',
].forEach((id) => {
    document.getElementById(id).addEventListener('input', reModify);
});

document.getElementById("the-canvas").addEventListener('click', console.log);
reModify();